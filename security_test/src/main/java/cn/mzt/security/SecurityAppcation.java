package cn.mzt.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityAppcation {
    public static void main(String[] args) {
        SpringApplication.run(SecurityAppcation.class,args);
    }
}
