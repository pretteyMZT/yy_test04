package cn.mzt.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/test")
public class PageController {

    @GetMapping("user/{name}")
    public String user(@PathVariable String name){
        return "/user/"+name;
    }

    @GetMapping("admin/{name}")
    public String admin(@PathVariable String name){
        return "/admin/"+name;
    }

    @GetMapping("{name}")
    public String name(@PathVariable String name){
        return name;
    }

    @GetMapping("index")
    public String index(){
        return "index";
    }

    @GetMapping("/userLogin")
    public String userLogin(){
        return "/userLogin";
    }
}
