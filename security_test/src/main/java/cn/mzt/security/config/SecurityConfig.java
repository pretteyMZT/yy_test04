package cn.mzt.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //设置放行的地址
//        http.authorizeRequests().mvcMatchers("/test/index","/test/admin").permitAll();

        http.authorizeRequests().mvcMatchers("/").hasAnyRole("USER","ADMIN");
        //设置访问的地址必须具备哪些角色
        http.authorizeRequests().mvcMatchers("/test/user/**").hasAnyRole("USER","ADMIN");
        http.authorizeRequests().mvcMatchers("/test/admin/**").hasAnyRole("ADMIN");

        //设置登录,并指定自定义的登录页面,指定自定义的账号和密码
        http.formLogin().loginPage("/test/userLogin").usernameParameter("username").passwordParameter("pwd");

        //记住我
        http.rememberMe().rememberMeParameter("reb");

        //设置全选不够的时候去指定的页面
        http.exceptionHandling().accessDeniedPage("/403");
    }

    //认证和授权
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
                .withUser("admin")
                .password(new BCryptPasswordEncoder().encode("1234")).roles("ADMIN")
                .and()
                .withUser("user").password(new BCryptPasswordEncoder().encode("1234")).roles("USER");

    }
}
